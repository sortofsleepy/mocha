#pragma once
#include "cinder/gl/gl.h"
#include "cinder/CameraUi.h"
#include "cinder/Timeline.h"
#include "cinder/Camera.h"
#include "cinder/app/App.h"
namespace mocha {
	class Camera {
	public:
		Camera(float fov = 60.0f, float aspect = ci::app::getWindowAspectRatio(), float near = 0.1f, float far = 1000.0f);

		void setZoom(float zoom);
		void enableUI();
		void useCamera();
	private:
		ci::CameraPersp mCam;
		ci::CameraUi mCamUi;
		float fov;
		ci::vec3 position, target, eye, up;
	};
}