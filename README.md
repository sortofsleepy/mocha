Update
====

Consider this deprecated, but useable. Currently rebuilding and refactoring things and making one larger general repo that spans multiple platforms
You can see that here 
[https://gitlab.com/sortofsleepy/kanpai](https://gitlab.com/sortofsleepy/kanpai)



~~Just a bunch of mishmashed snippits of code that have been helpful when working with Cinder.
There shouldn't be any project files or library dependencies, you should be able to just drop 
in the files however you'd like and go.~~

~~Install~~
=====
* ~~Most of the files are header only, so just drag and drop the inner folder called "mocha" into wherever you're keeping your libraries. All of the necessary .cpp files can be found in "src". shaders should be dropped into wherever you keep shaders.~~
* ~~Please note that this is not necessarily in a functioning state at the moment. Some of the files are useable but may or may not require tweaks on your part.~~
 

~~TODO!~~
====
- ~~organize~~
- ~~fix and parameterize EnvMap~~
- ~~add proper attribution where necessary.~~
- ~~testing~~